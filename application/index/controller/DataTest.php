<?php

namespace app\index\controller;

use think\Controller;
use think\Db;

use app\index\model\User;

class DataTest extends Controller
{
    function index()
    {
        $data = User::select();
        return json($data);
        // return 'index';
    }

    function user()
    {

    }

    public function insert()
    {
        $data = [
            'name'         => '辉夜',
            'password'     => '123456',
            'gender'       => '女',
            'email'        => '123@a.com',
            'price'        => 90,
//            'address'      => '123',
            'created_time' => date('Y-m-d H:i:s', time())
        ];

    }

    public function update()
    {
        $data = [
            'username'  => '辉夜姬',
            'id'    => 233,
            'price' => Db::raw('price - 2 + 3')
        ];
//        Db::name('user')->update($data);
        Db::name('user')->where('id', 233)->setInc('price', 12);
    }
}