<?php
namespace app\index\model;
use think\Model;

class User extends Model
{
    // 邮箱模糊查询的搜索器
    public function searchEmailAttr($query, $value)
    {
        $query->where('email', 'like', $value. "%");
    }
}